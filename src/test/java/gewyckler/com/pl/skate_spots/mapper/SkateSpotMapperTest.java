package gewyckler.com.pl.skate_spots.mapper;

import gewyckler.com.pl.skate_spots.model.SkateSpot;
import gewyckler.com.pl.skate_spots.model.dto.SkateSpotDto;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.Arrays;

class SkateSpotMapperTest {
    private SkateSpotMapper mapper = Mappers.getMapper(SkateSpotMapper.class);

    @Test
    public void test_mapingDtoIntoDatabaseObject() {
        SkateSpotDto dto = createSkateSpotDto();
        SkateSpot skateSpot = mapper.dtoToSkateSpot(dto);

        Assert.assertEquals(dto.getSkateSpotId(), skateSpot.getSkateSpotId());
        Assert.assertEquals(dto.getName(), skateSpot.getName());
        Assert.assertEquals(dto.getSkateSpotType(), skateSpot.getSkateSpotType());
        Assert.assertEquals(dto.isPaid(), skateSpot.isPaid());
        Assert.assertEquals(dto.isLightning(), skateSpot.isLightning());
        Assert.assertEquals(dto.isIndoor(), skateSpot.isIndoor());
        Assert.assertEquals(dto.isAccepted(), skateSpot.isAccepted());
        Assert.assertEquals(dto.getCity(), skateSpot.getCity());
        Assert.assertEquals(dto.getAddress(), skateSpot.getAddress());
        Assert.assertEquals(dto.getCountry(), skateSpot.getCountry());
        Assert.assertEquals(dto.getLatX(), skateSpot.getLatX());
        Assert.assertEquals(dto.getLonY(), skateSpot.getLonY());
        Assert.assertEquals(dto.getListOfObstacle(), skateSpot.getListOfObstacle());
    }

    @Test
    public void test_mapingDatabaseObjectIntoDto() {
        SkateSpot skateSpot = createSkateSpot();
        SkateSpotDto dto = mapper.skateSpotToDto(skateSpot);

        Assert.assertEquals(skateSpot.getSkateSpotId(), dto.getSkateSpotId());
        Assert.assertEquals(skateSpot.getName(), dto.getName());
        Assert.assertEquals(skateSpot.getSkateSpotType(), dto.getSkateSpotType());
        Assert.assertEquals(skateSpot.isPaid(), dto.isPaid());
        Assert.assertEquals(skateSpot.isLightning(), dto.isLightning());
        Assert.assertEquals(skateSpot.isIndoor(), dto.isIndoor());
        Assert.assertEquals(skateSpot.isAccepted(), dto.isAccepted());
        Assert.assertEquals(skateSpot.getCity(), dto.getCity());
        Assert.assertEquals(skateSpot.getAddress(), dto.getAddress());
        Assert.assertEquals(skateSpot.getCountry(), dto.getCountry());
        Assert.assertEquals(skateSpot.getLatX(), dto.getLatX());
        Assert.assertEquals(skateSpot.getLonY(), dto.getLonY());
        Assert.assertEquals(skateSpot.getListOfObstacle(), dto.getListOfObstacle());
    }

    private SkateSpotDto createSkateSpotDto() {
        SkateSpotDto dto = new SkateSpotDto();
        dto.setSkateSpotId(1L);
        dto.setName("Name");
        dto.setSkateSpotType("SKATEPARK");
        dto.setPaid(false);
        dto.setLightning(false);
        dto.setIndoor(false);
        dto.setAccepted(false);
        dto.setAccepted(false);
        dto.setCity("City");
        dto.setAddress("Street, 00-000 City, Country");
        dto.setLatX(10.20);
        dto.setLonY(20.10);
        dto.setListOfObstacle(Arrays.asList("BANK", "BOX"));
        return dto;
    }

    private SkateSpot createSkateSpot() {
        SkateSpot skateSpot = new SkateSpot();
        skateSpot.setSkateSpotId(1L);
        skateSpot.setName("Name");
        skateSpot.setSkateSpotType("SKATEPARK");
        skateSpot.setPaid(false);
        skateSpot.setLightning(false);
        skateSpot.setIndoor(false);
        skateSpot.setAccepted(false);
        skateSpot.setAccepted(false);
        skateSpot.setCity("City");
        skateSpot.setAddress("Street, 00-000 City, Country");
        skateSpot.setLatX(10.20);
        skateSpot.setLonY(20.10);
        skateSpot.setListOfObstacle(Arrays.asList("BANK", "BOX"));
        return skateSpot;
    }

}