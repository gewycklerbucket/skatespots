//package gewyckler.com.pl.skate_spots;
//
//import gewyckler.com.pl.skate_spots.model.SkateSpot;
//import gewyckler.com.pl.skate_spots.model.dto.SkateSpotDto;
//import gewyckler.com.pl.skate_spots.model.enums.SkateSpotType;
//import org.junit.Assert;
//import org.junit.FixMethodOrder;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.junit.runners.MethodSorters;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.core.ParameterizedTypeReference;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import java.util.Arrays;
//import java.util.List;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
//@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
//class SkateSpotsApplicationTests {
//
//
//    @Autowired
//    private TestRestTemplate testRestTemplate;
//    private final String NAME = "Schodki";
//    private final String SKATE_SPOT_TYPE = SkateSpotType.SKATEPARK.toString();
//    private final boolean IS_PUBLIC = true;
//    private final String CITY = "WARSAW";
//    private final String ADRESS = "Ulicowa";
//    private final String COUNTRY = "Polska";
//    private final Double LAT_X = 39.32231;
//    private final Double LON_Y = 39.45412;
//    private final List<String> OBSTACLELIST = Arrays.asList("BOX", "RAIL");
//    private final String IMAGE = "5ASD58DS5sdn8";
//
//
//    @Test
//    void test_1_add_skateSpot() {
//        SkateSpotDto skateSpotCreateRequest = new SkateSpotDto(
//                null,
//                NAME,
//                SKATE_SPOT_TYPE,
//                IS_PUBLIC,
//                CITY,
//                ADRESS,
//                COUNTRY,
//                LAT_X,
//                LON_Y,
//                OBSTACLELIST,
//                IMAGE);
//
//        HttpEntity<SkateSpotDto> httpEntity = new HttpEntity<>(skateSpotCreateRequest);
//
//        ResponseEntity<Long> response = testRestTemplate.exchange(
//                "/skatespot",
//                HttpMethod.PUT,
//                httpEntity,
//                new ParameterizedTypeReference<Long>() {
//                });
//
//        Long createdId = response.getBody();
//        Assert.assertNotNull(createdId);
//    }
//
//    @Test
//    void test_2_check_added_skateSpot() {
//        ResponseEntity<List<SkateSpot>> response = getSkateSpotList();
//        List<SkateSpot> skateSpotList = response.getBody();
//
//        Assert.assertNotNull(skateSpotList);
//        Assert.assertFalse(skateSpotList.isEmpty());
//        Assert.assertEquals(1, skateSpotList.size());
//
//        SkateSpot retrieved = skateSpotList.get(0);
//
//        Assert.assertEquals(NAME, retrieved.getName());
//        Assert.assertEquals(SKATE_SPOT_TYPE, retrieved.getSkateSpotType());
//        Assert.assertEquals(CITY, retrieved.getCity());
//        Assert.assertEquals(ADRESS, retrieved.getAddress());
//        Assert.assertEquals(COUNTRY, retrieved.getCountry());
//        Assert.assertEquals(LAT_X, retrieved.getLatX());
//        Assert.assertEquals(LON_Y, retrieved.getLonY());
//    }
//
//    @Test
//    void test_3_get_skateSpot_by_id() {
//        ResponseEntity<List<SkateSpot>> responseGet = getSkateSpotList();
//
//        Long skateSpotIdToGet = responseGet.getBody().get(0).getSkateSpotId();
//
//        ResponseEntity<SkateSpot> skateSpotGet = testRestTemplate.exchange("/skatespot/" + skateSpotIdToGet,
//                HttpMethod.GET,
//                null,
//                new ParameterizedTypeReference<SkateSpot>() {
//                });
//
//        Assert.assertEquals(HttpStatus.OK, skateSpotGet.getStatusCode());
//        Assert.assertEquals(skateSpotIdToGet, skateSpotGet.getBody().getSkateSpotId());
//    }
//
//    @Test
//    void test_4_get_skateSpot_by_city() {
//        ResponseEntity<List<SkateSpot>> responseGet = getSkateSpotList();
//        List<SkateSpot> skateSpotList = responseGet.getBody();
//        String skateSpotCityToGet = skateSpotList.get(0).getCity();
//
//        responseGet = testRestTemplate.exchange("/skatespot/city/" + skateSpotCityToGet,
//                HttpMethod.GET,
//                null, new ParameterizedTypeReference<List<SkateSpot>>() {
//                });
//
//        skateSpotList = responseGet.getBody();
//        Assert.assertEquals(HttpStatus.OK, responseGet.getStatusCode());
//        skateSpotList.stream().forEach(s -> s.getCity().equals(skateSpotCityToGet));
//    }
//
//
//    @Test
//    void test_5_delete_skateSpot_by_id() {
//        ResponseEntity<List<SkateSpot>> responseGet = getSkateSpotList();
//
//        List<SkateSpot> skateSpotList = responseGet.getBody();
//        Long idToDelete = skateSpotList.get(0).getSkateSpotId();
//
//        ResponseEntity<SkateSpot> responseDelete = testRestTemplate.exchange("/skatespot/" + idToDelete,
//                HttpMethod.DELETE,
//                null,
//                new ParameterizedTypeReference<SkateSpot>() {
//                });
//
//        Assert.assertEquals(HttpStatus.ACCEPTED, responseDelete.getStatusCode());
//
//        responseGet = testRestTemplate.exchange("/skatespot",
//                HttpMethod.GET,
//                null,
//                new ParameterizedTypeReference<List<SkateSpot>>() {
//                });
//        skateSpotList = responseGet.getBody();
//        Assert.assertEquals(HttpStatus.OK, responseGet.getStatusCode());
//        skateSpotList.stream().noneMatch(s -> s.getSkateSpotId().equals(idToDelete));
//
//    }
//
//    private ResponseEntity<List<SkateSpot>> getSkateSpotList() {
//        ResponseEntity<List<SkateSpot>> responseGet = testRestTemplate.exchange("/skatespot",
//                HttpMethod.GET,
//                null,
//                new ParameterizedTypeReference<List<SkateSpot>>() {
//                });
//        return responseGet;
//    }
//
//}
