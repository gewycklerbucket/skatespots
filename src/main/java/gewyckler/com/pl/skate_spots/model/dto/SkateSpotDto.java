package gewyckler.com.pl.skate_spots.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SkateSpotDto {

    private Long skateSpotId;
    private String name;
    private String skateSpotType;

    private boolean paid;
    private boolean lightning;
    private boolean indoor;
    private boolean accepted;

    private String city;
    private String address;
    private String country;

    private Double latX;
    private Double lonY;

    private List<String> listOfObstacle = new ArrayList<>();

    private String image;

}