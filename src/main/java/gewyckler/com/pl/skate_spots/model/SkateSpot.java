package gewyckler.com.pl.skate_spots.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Main class SkateSpot.
 */

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class SkateSpot {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ApiModelProperty(value = "The database generated SkateSpot ID")
    private Long skateSpotId;

    @Nullable
    @ApiModelProperty(value = "The name of SkateSpot, if none set the 'Default Name'")
    private String name;

    @ApiModelProperty(value = "The type of SkateSpot, SKATEPARK or STREET SPOT")
    private String skateSpotType;

    @ApiModelProperty(value = "The information that SkateSpot is paid or is for free")
    private boolean paid;

    @ApiModelProperty(value = "The information that SkateSpot is illuminated")
    private boolean lightning;

    @ApiModelProperty(value = "The information that SkateSpot is indoor (rain protected)")
    private boolean indoor;

    @ApiModelProperty(value = "Variable specify if the SkateSpot is accepted to put on map")
    private boolean accepted;

    @ApiModelProperty(value = "The name of the city SkateSpot is located")
    private String city;

    @ApiModelProperty(value = "The address of the SkateSpot location (street name, building number, post code, city, country)")
    private String address;

    @ApiModelProperty(value = "The name of the country SkateSpot is located")
    private String country;

    @ApiModelProperty(value = "The latitude of the SkateSpot location")
    private Double latX;
    @ApiModelProperty(value = "The longitude of the SkateSpot location")
    private Double lonY;

    @ElementCollection(fetch = FetchType.EAGER)
    @ApiModelProperty(value = "The list of obstacles that are available on SkateSpot")
    @CollectionTable(name = "obstacleList", joinColumns = @JoinColumn(name = "id"))
    private List<String> listOfObstacle = new ArrayList<>();

    @Lob
    @Nullable
    @ApiModelProperty(value = "The images of SkateSpot")
    @Column(columnDefinition = "LONGBLOB")
    private byte[] image;
}
