package gewyckler.com.pl.skate_spots.model.enums;

public enum ObstacleType {
    BANK,
    BOWL,
    BOX,
    GRIND_BOX,
    MINI_RAMP,
    PYRAMID,
    RAIL,
    STAIRS,
    QUARTER_PIPE,
}
