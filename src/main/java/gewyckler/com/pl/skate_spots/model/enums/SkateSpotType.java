package gewyckler.com.pl.skate_spots.model.enums;

public enum SkateSpotType {
    SKATEPARK,
    STREET_SPOT,
}
