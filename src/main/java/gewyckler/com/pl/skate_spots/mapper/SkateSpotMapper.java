package gewyckler.com.pl.skate_spots.mapper;

import gewyckler.com.pl.skate_spots.model.SkateSpot;
import gewyckler.com.pl.skate_spots.model.dto.SkateSpotDto;
import org.mapstruct.Mapper;

import java.util.Base64;
import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public abstract class SkateSpotMapper {

    public abstract List<SkateSpotDto> skateSpotToDto(Collection<SkateSpot> skateSpots);


    public SkateSpotDto skateSpotToDto(SkateSpot skateSpots) {

        SkateSpotDto skateSpotDto = new SkateSpotDto();

        if (skateSpots.getSkateSpotId() != null) {
            skateSpotDto.setSkateSpotId(skateSpots.getSkateSpotId());
        }

        skateSpotDto.setName(skateSpots.getName());
        skateSpotDto.setSkateSpotType(skateSpots.getSkateSpotType());
        skateSpotDto.setLightning(skateSpots.isLightning());
        skateSpotDto.setIndoor(skateSpots.isIndoor());
        skateSpotDto.setPaid(skateSpots.isPaid());

        skateSpotDto.setAccepted(skateSpots.isAccepted());

        skateSpotDto.setCity(skateSpots.getCity());
        skateSpotDto.setAddress(skateSpots.getAddress());
        skateSpotDto.setCountry(skateSpots.getCountry());
        skateSpotDto.setLatX(skateSpots.getLatX());
        skateSpotDto.setLonY(skateSpots.getLonY());
        skateSpotDto.setListOfObstacle(skateSpots.getListOfObstacle());
        if (skateSpots.getImage() != null && skateSpots.getImage().length > 0) {
            skateSpotDto.setImage(Base64.getEncoder().encodeToString(skateSpots.getImage()));
        }

        return skateSpotDto;
    }

    public SkateSpot dtoToSkateSpot(SkateSpotDto dto) {

        SkateSpot skateSpot = new SkateSpot();

        if (dto.getSkateSpotId() != null) {
            skateSpot.setSkateSpotId(dto.getSkateSpotId());
        }

        skateSpot.setName(dto.getName());
        skateSpot.setSkateSpotType(dto.getSkateSpotType());
        skateSpot.setLightning(dto.isLightning());
        skateSpot.setIndoor(dto.isIndoor());
        skateSpot.setPaid(dto.isPaid());
        skateSpot.setCity(dto.getCity());
        skateSpot.setAddress(dto.getAddress());
        skateSpot.setCountry(dto.getCountry());
        skateSpot.setLatX(dto.getLatX());
        skateSpot.setLonY(dto.getLonY());
        skateSpot.setListOfObstacle(dto.getListOfObstacle());

        if (dto.getImage() != null && !dto.getImage().isEmpty()) {
            skateSpot.setImage(Base64.getDecoder().decode(dto.getImage()));
        }

        return skateSpot;
    }
}
