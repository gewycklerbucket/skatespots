package gewyckler.com.pl.skate_spots.repository;

import gewyckler.com.pl.skate_spots.model.SkateSpot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SkateSpotRepository extends JpaRepository<SkateSpot, Long> {
    Optional<List<SkateSpot>> findByCity(String city);

    Optional<List<SkateSpot>> findBySkateSpotType(String skateSpotType);

    Optional<List<SkateSpot>> findByName(String skateSpotName);

    Optional<List<SkateSpot>> findByListOfObstacleIsLike(String obstacleName);
}
