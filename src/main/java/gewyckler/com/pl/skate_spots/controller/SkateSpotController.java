package gewyckler.com.pl.skate_spots.controller;

import gewyckler.com.pl.skate_spots.model.SkateSpot;
import gewyckler.com.pl.skate_spots.model.dto.SkateSpotDto;
import gewyckler.com.pl.skate_spots.service.SkateSpotService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * SkateSpot class controller
 */
@RestController
@RequestMapping(path = "/skatespot")
@AllArgsConstructor
@Api(value = "skatespotdatabase", description = "Operations pertaining to skate spot places in SkateSpot database")
@CrossOrigin(origins = "http://localhost:3000")
public class SkateSpotController {

    private final SkateSpotService skateSpotService;

    // GET
    @ApiOperation(value = "Get all Skate Spots")
    @GetMapping
    public List<SkateSpotDto> getSkateSpotsList() {
        return skateSpotService.getAll();
    }

    @ApiOperation(value = "Get all Skate Spots with given name")
    @GetMapping("/name/{name}")
    private List<SkateSpotDto> getSkateSpotsByName(@PathVariable("name") String name) {
        return skateSpotService.getByName(name);
    }

    @ApiOperation(value = "Get all Skate Spots located in given city")
    @GetMapping("/city/{city}")
    public List<SkateSpotDto> getSkateSpotsByCity(@PathVariable("city") String city) {
        return skateSpotService.getByCity(city);
    }

    @ApiOperation(value = "Get all Skate Spots of given type")
    @GetMapping("/type/{skateSpotType}")
    public List<SkateSpotDto> getSkateSpotsByType(@PathVariable("skateSpotType") String skateSpotType) {
        return skateSpotService.getByType(skateSpotType);
    }

    @ApiOperation(value = "Get all Skate Spots with given obstacle")
    @GetMapping("/obstacle/{obstacleType}")
    public List<SkateSpotDto> getSkateSpotByObstacleType(@PathVariable("obstacleType") String obstacleType) {
        return skateSpotService.getByObstacleType(obstacleType);
    }

    // POST
    @ApiOperation(value = "Create new Skate Spot")
    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public SkateSpot createSkateSpot(@RequestBody SkateSpotDto skateSpotDto) {
        return skateSpotService.save(skateSpotDto);
    }


    // Put
    @ApiOperation(value = "Update existing Skate Spot with given ID")
    @PutMapping
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void updateSkateSpot(@RequestBody SkateSpotDto skateSpotUpdateRequest) {
        skateSpotService.update(skateSpotUpdateRequest);
    }

    @ApiOperation(value = "Accept existing Skate Spot to be get from database")
    @PutMapping("accept/{skateSpotId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void acceptSkateSpot(@PathVariable("skateSpotId") Long skateSpotId) {
        skateSpotService.acceptSkateSpot(skateSpotId);
    }

    // DELETE
    @ApiOperation(value = "Delete existing Skate Spot with given ID")
    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteSkateSpot(@PathVariable("id") Long skateSpotId) {
        skateSpotService.deleteById(skateSpotId);
    }
}
