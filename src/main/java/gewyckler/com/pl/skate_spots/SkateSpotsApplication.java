package gewyckler.com.pl.skate_spots;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkateSpotsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SkateSpotsApplication.class, args);
    }

}
