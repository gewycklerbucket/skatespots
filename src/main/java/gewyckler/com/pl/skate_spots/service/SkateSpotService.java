package gewyckler.com.pl.skate_spots.service;

//import gewyckler.com.pl.skate_spots.mapper.ObstacleMapper;

//import gewyckler.com.pl.skate_spots.mapper.ObstacleMapper;

import gewyckler.com.pl.skate_spots.mapper.SkateSpotMapper;
import gewyckler.com.pl.skate_spots.model.SkateSpot;
import gewyckler.com.pl.skate_spots.model.dto.SkateSpotDto;
import gewyckler.com.pl.skate_spots.repository.SkateSpotRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

/**
 * SkateSpot class service
 */
@Service
@AllArgsConstructor
public class SkateSpotService {

    private final SkateSpotRepository skateSpotRepository;
    private final SkateSpotMapper skateSpotMapper;

    public List<SkateSpotDto> getAll() {
        return skateSpotMapper.skateSpotToDto(skateSpotRepository.findAll());
    }

    public List<SkateSpotDto> getByName(String name) {
        Optional<List<SkateSpot>> optionalSkateSpot = skateSpotRepository.findByName(name);
        if (optionalSkateSpot.isPresent()) {
            return skateSpotMapper.skateSpotToDto(optionalSkateSpot.get());
        }
        throw new EntityNotFoundException("skatespot, id: " + name);
    }

    public List<SkateSpotDto> getByCity(String city) {
        Optional<List<SkateSpot>> optionalSkateSpots = skateSpotRepository.findByCity(city);
        if (optionalSkateSpots.isPresent()) {
            return skateSpotMapper.skateSpotToDto(optionalSkateSpots.get());

        }
        throw new EntityNotFoundException("skatespot, city: " + city);
    }

    public List<SkateSpotDto> getByType(String skateSpotType) {
        Optional<List<SkateSpot>> optionalSkateSpots = skateSpotRepository.findBySkateSpotType(skateSpotType);
        if (optionalSkateSpots.isPresent()) {
            return skateSpotMapper.skateSpotToDto(optionalSkateSpots.get());
        }
        throw new EntityNotFoundException("skatespot, skateSpotType: " + skateSpotType);
    }

    public List<SkateSpotDto> getByObstacleType(String obstacleType) {
        Optional<List<SkateSpot>> optionalSkateSpot = skateSpotRepository.findByListOfObstacleIsLike(obstacleType);
        if (optionalSkateSpot.isPresent()) {

            return skateSpotMapper.skateSpotToDto(optionalSkateSpot.get());
        }
        throw new EntityNotFoundException("skatespot, skatespot with: " + obstacleType);
    }

    public SkateSpot save(SkateSpotDto dto) {

        SkateSpot skateSpot = skateSpotMapper.dtoToSkateSpot(dto);
        skateSpotRepository.save(skateSpot);
        return skateSpot;
    }

    /**
     * Checking is the given parameter exist and if does, assign given values (that are not null) to right fields.
     *
     * @param givenSkateSpot
     */
    public void update(SkateSpotDto givenSkateSpot) {
        Optional<SkateSpot> optionalSkateSpot = skateSpotRepository.findById(givenSkateSpot.getSkateSpotId());
        if (optionalSkateSpot.isPresent()) {
            SkateSpot skateSpotToEdit = optionalSkateSpot.get();
            if (!givenSkateSpot.getName().isEmpty()
                    && !givenSkateSpot.getName().equals(skateSpotToEdit.getName())) {
                skateSpotToEdit.setName(givenSkateSpot.getName());
            }
            if (!givenSkateSpot.getSkateSpotType().isEmpty()
                    && !givenSkateSpot.getSkateSpotType().equals(skateSpotToEdit.getSkateSpotType())) {
                skateSpotToEdit.setSkateSpotType((givenSkateSpot.getSkateSpotType()));
            }
            if (!givenSkateSpot.getAddress().isEmpty()
                    && !givenSkateSpot.getAddress().equals(skateSpotToEdit.getAddress())) {
                skateSpotToEdit.setAddress(givenSkateSpot.getAddress());
            }
            if (givenSkateSpot.isPaid() != skateSpotToEdit.isPaid()) {
                skateSpotToEdit.setPaid(givenSkateSpot.isPaid());
            }
            if (givenSkateSpot.isLightning() != skateSpotToEdit.isLightning()) {
                skateSpotToEdit.setLightning(givenSkateSpot.isLightning());
            }
            if (givenSkateSpot.isIndoor() != skateSpotToEdit.isIndoor()) {
                skateSpotToEdit.setIndoor(givenSkateSpot.isIndoor());
            }
            if (!givenSkateSpot.getCity().isEmpty()
                    && !givenSkateSpot.getCity().equals(skateSpotToEdit.getCity())) {
                skateSpotToEdit.setCity(givenSkateSpot.getCity());
            }
            if (givenSkateSpot.getLatX() != skateSpotToEdit.getLatX()) {
                skateSpotToEdit.setLatX(givenSkateSpot.getLatX());
            }
            if (givenSkateSpot.getLonY() != skateSpotToEdit.getLonY()) {
                skateSpotToEdit.setLonY(givenSkateSpot.getLonY());
            }
            if (!givenSkateSpot.getCountry().isEmpty()
                    && !givenSkateSpot.getCountry().equals(skateSpotToEdit.getCountry())) {
                skateSpotToEdit.setCountry(givenSkateSpot.getCountry());
            }

            skateSpotRepository.save(skateSpotToEdit);
            return;
        }
        throw new EntityNotFoundException("skatespot, id: " + givenSkateSpot.getSkateSpotId());
    }

    public void acceptSkateSpot(Long skateSpotId) {
        Optional<SkateSpot> optionalSkateSpot = skateSpotRepository.findById(skateSpotId);
        if (optionalSkateSpot.isPresent()) {
            SkateSpot skateSpot = optionalSkateSpot.get();
            skateSpot.setAccepted(true);
            skateSpotRepository.save(skateSpot);
        }
    }

    public void deleteById(Long skateSpotId) {
        if (skateSpotRepository.existsById(skateSpotId)) {
            skateSpotRepository.deleteById(skateSpotId);
            return;
        }
        throw new EntityNotFoundException("skatespot, id: " + skateSpotId);
    }
}
